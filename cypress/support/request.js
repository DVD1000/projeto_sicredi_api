Cypress.Commands.add('consultaCEPValido', () => {
    return cy.request({
        method: 'GET',
        url: '91060900/json/',
        failOnStatusCode: false
    })
})

Cypress.Commands.add('consultaCEPInexistente', () => {
    return cy.request({
        method: 'GET', 
        url: '91060901/json',
        failOnStatusCode: false
    })
})

Cypress.Commands.add('consultaCEPFormatoInvalido', () => {
    return cy.request({        
        method: 'GET',
        url: '9106090LLOOII/json',
        failOnStatusCode: false
    })
})

Cypress.Commands.add('validarRetornoExtra', () => {
    return cy.request({        
        method: 'GET',
        url: 'RS/Gravata/Barroso/json',            
        failOnStatusCode: false,
        headers: {
            'accept': 'application/json'
        }
    })
})