/// <reference types="cypress" />

describe('Consultar cenários extra...', () => {    
    it('Realizar a consulta dos CEPs...', () => {  
        cy.fixture('dados').then(dados => {
            expect(dados.cep).to.eq("94085-170")
            expect(dados.logradouro).to.equal("Rua Ari Barroso")
            expect(dados.bairro).to.equal("Morada do Vale I")
        })      
        cy.validarRetornoExtra().should((response) => {  
            expect(response.status).to.equal(200)            
            expect(response.body).to.not.be.null;
            cy.log(JSON.stringify(response.body))
        })        
    })       
})






  