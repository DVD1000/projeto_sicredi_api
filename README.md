# Projeto Sicredi API

Neste projeto de API foi utilizado o Cypress em JavaScript para realização da automação da API.

Além disso abaixo segue o link do Cypress para maiores informações sobre este framework que tenho utilizado em outros projetos de API e E2E.

**Link do Cypress:** [Cypress](https://cypress.io)

## O Projeto:
Neste projeto foi para realizar a validação de três cenários e mais um cenário extra:

[URL deste Projeto](https://viacep.com.br/ws/91060900/json/)

Cenário: Consulta CEP valido
Dado que o usuário inseri um CEP válido
Quando o serviço é consultado
Então é retornado o CEP, logradouro, complemento, bairro, localidade, uf e ibge.

Cenário: Consulta CEP inexistente
Dado que o usuário inseri um CEP que não exista na base dos Correios
Quando o serviço é consultado
Então é retornada um atributo erro

Cenário: Consulta CEP com formato inválido
Dado que o usuário inseri um CEP com formato inválido
Quando o serviço é consultado
Então é retornado uma mensagem de erro

Extras:
1) Criar um cenário que verifique o retorno do serviço abaixo:
URL: https://viacep.com.br/ws/RS/Gravatai/Barroso/json/

**Em todos estes cenários foram utilizados o item de page objects separando a chamada da API do método: GET e esta chamada sendo realizada dentro da spec dos testes na pasta integration.**

## Estrutura de Pastas do Projeto:
A estrutura deste projeto no Cypress possui as seguintes características:

- `fixtures`: é onde são chamadas a massa de testes, neste caso os dados da API para realizar a validação (asserts);
- `Integration`: é onde consta as specs dos testes deste projeto.
- `support`: é onde separamos a parte da chamada do Page Objects num projeto de automação. Neste caso, montei um arquivo em JS - request.js para realizar esta chamadas da API, pois se quisermos (por exemplo) usar esta chamadas em outras specs, não iremos necessitar de codificar novamente, só realizar a chamadas deste método do Cypress: Cypress.Commads.add 

## Como rodar o projeto:

Ao baixar o projeto em sua máquina local, abra o terminal ou shell de sua máquina e rode os seguintes comandos:

Para baixar o projeto:
```
git clone
```

Para baixar as dependências do Cypress e o node_modules:
````
npm i
````

Para rodar o projeto do Cypress: Cypress
````
npm test
````
**OBS:** Com o comando acima, irá rodar o cypress no formato visual, onde irá exibir um model com as quatro specs dos testes criadas. Sendo assim, só será necessário clicar na spec do teste para rodar a automação..

**E assim o Cypress irá abrir um model onde você poderá rodar o projeto de forma visual**

Se preferir via headless(shell):

````
npx cypress run
````
